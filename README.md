# Aplicatia bancara - Kaleido 

Aplicatia bancara Kaleido reprezinta un model de aplicatie home banking. Aplicatia are urmatoarele functionalizati: creare cont, logare in cont, vizualizare date personale, vizualizare sold cont curent, cont de economii, tranzactii bancare si cereri de credit

## Adresa repository

adresa: https://gitlab.upt.ro/antonia.itineant/aplicatia-bancara-kaleido


## Aplicatii necesare

- NetBeans 8.2 
- MySQL 


## Pasi compilare proiect 
1. Se deschide proiectul Banking-System in NetBeans 8.2
2. Se descarca si importa baza de date  - banking 
3. Se deschide fiserul LoginPage.java si se executa cu butonul de runfile 
4. Se foloseste ca si cont de testare urmatorul user:  
numecont: itineantantonia05
parola: Antonia03!
5. Se poate testa:
- functia de resetare a parolei
- functia de tranzactii bancare 
- functia de vizualizare date cont 
- functia de servicii bancare

