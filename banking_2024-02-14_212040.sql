/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET SQL_NOTES=0 */;
DROP TABLE IF EXISTS cardcumparaturi;
CREATE TABLE `cardcumparaturi` (
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS cardeconomii;
CREATE TABLE `cardeconomii` (
  `userid` varchar(225) NOT NULL,
  `numecont` varchar(225) DEFAULT NULL,
  `iban` varchar(225) DEFAULT NULL,
  `suma` int DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS kaleidoextra;
CREATE TABLE `kaleidoextra` (
  `motiv` varchar(225) NOT NULL,
  `tip_rata` varchar(225) NOT NULL,
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS kaleidopersonal;
CREATE TABLE `kaleidopersonal` (
  `motiv` varchar(225) NOT NULL,
  `suma_rata` varchar(225) NOT NULL,
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS plataconturi;
CREATE TABLE `plataconturi` (
  `detalii` varchar(225) NOT NULL,
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS platafacturi;
CREATE TABLE `platafacturi` (
  `dataora` varchar(225) NOT NULL,
  `factura` varchar(225) NOT NULL,
  `iban` varchar(225) NOT NULL,
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS platalei;
CREATE TABLE `platalei` (
  `numebeneficiar` varchar(225) NOT NULL,
  `banca` varchar(225) NOT NULL,
  `iban` varchar(225) NOT NULL,
  `detalii` varchar(225) NOT NULL,
  `dataora` varchar(225) NOT NULL,
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS plataprieteni;
CREATE TABLE `plataprieteni` (
  `dataora` varchar(225) NOT NULL,
  `nr_telefon` varchar(225) NOT NULL,
  `nume` varchar(225) NOT NULL,
  `cont` varchar(225) NOT NULL,
  `suma` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS sumacont;
CREATE TABLE `sumacont` (
  `userid` varchar(225) NOT NULL,
  `numecont` varchar(225) DEFAULT NULL,
  `iban` varchar(225) DEFAULT NULL,
  `suma` int DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS userid;
CREATE TABLE `userid` (
  `userid` varchar(225) NOT NULL,
  `iban` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS utilizatori;
CREATE TABLE `utilizatori` (
  `nume` varchar(225) DEFAULT NULL,
  `prenume` varchar(225) DEFAULT NULL,
  `userid` varchar(225) NOT NULL,
  `numecont` varchar(225) DEFAULT NULL,
  `parola` varchar(225) DEFAULT NULL,
  `iban` varchar(225) DEFAULT NULL,
  `serie` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `adresa` varchar(225) DEFAULT NULL,
  `nrtelefon` varchar(225) DEFAULT NULL,
  `datanasterii` varchar(225) DEFAULT NULL,
  `dataexpirare` varchar(225) DEFAULT NULL,
  `tara` varchar(225) DEFAULT NULL,
  `cetatenie` varchar(225) DEFAULT NULL,
  `sursa` varchar(225) DEFAULT NULL,
  `ocupatie` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO cardcumparaturi(suma) VALUES('5000');

INSERT INTO cardeconomii(userid,numecont,iban,suma) VALUES('\'IA288114\'','\'itineantantonia05\'','\'RO61KALB3333777742359999\'','7000'),('\'PD481667\'','\'pintiliedenis02\'','\'RO67KALB3333777790903434\'','21050');

INSERT INTO kaleidoextra(motiv,tip_rata,suma) VALUES('\'casa\'','\'4 rate\'','20000'),('\'casa\'','\'12 rate\'','20000');

INSERT INTO kaleidopersonal(motiv,suma_rata,suma) VALUES('\'sanatate personala\'','\'3500 lei\'','10000');

INSERT INTO plataconturi(detalii,suma) VALUES('\'\'','NULL'),('\'economii\'','600'),('\'economii\'','4850');

INSERT INTO platafacturi(dataora,factura,iban,suma) VALUES('\'13/02/2024 13:31:21\'','\'Internet\'','\'RO98KALB3333777798989898\'','30'),('\'13/02/2024 15:10:58\'','\'Apa\'','\'RO98KALB3333777798989898\'','50');

INSERT INTO platalei(numebeneficiar,banca,iban,detalii,dataora,suma) VALUES('\'\'','\'\'','\'\'','\'\'','\'25/01/2024 12:43:35\'','NULL'),('\'OsoianAdina\'','\'Banca Transilvania\'','\'RO45BTRL1234567676789890\'','\'cadouuu\'','\'13/02/2024 22:03:56\'','100'),('\'Iulian\'','\'BRD\'','\'RO56BRDR9087263456253656\'','\'joc\'','\'13/02/2024 22:10:36\'','50');

INSERT INTO plataprieteni(dataora,nr_telefon,nume,cont,suma) VALUES('\'13/02/2024 10:30:02\'','\'0785632145\'','\'RO45BRDR9823777724536723\'','\'PintilieDenis\'','100'),('\'13/02/2024 22:22:03\'','\'0727485016\'','\'RO67BCRR8987898989767676\'','\'Viorel Pintilie\'','500');

INSERT INTO sumacont(userid,numecont,iban,suma) VALUES('\'IA288114\'','\'itineantantonia05\'','\'RO98KALB3333777798989898\'','32100'),('\'PD481667\'','\'pintiliedenis02\'','\'RO67KALB3333777765654545\'','24500');

INSERT INTO userid(userid,iban) VALUES('\'IA288114\'','\'RO98KALB3333777798989898\''),('\'PD481667\'','\'RO67KALB3333777765654545\'');
INSERT INTO utilizatori(nume,prenume,userid,numecont,parola,iban,serie,email,adresa,nrtelefon,datanasterii,dataexpirare,tara,cetatenie,sursa,ocupatie) VALUES('\'Itineant\'','\'Antonia\'','\'IA288114\'','\'itineantantonia05\'','\'9bf9ca457644f992a41f591097fe144cf118e8ad7edcb3c7bec23711267174c0\'','\'RO98KALB3333777798989898\'','\'TZ-674\'','\'itineant.antoniaaaa@gmail.com\'','\'General Ion Dragalina\'','\'0727485016\'','\'30/05/1998\'','\'30/05/2030\'','\'Romania\'','\'roman\'','\'contracte drepturi de autor\'','\'artist independent\''),('\'Pintilie\'','\'Denis\'','\'PD481667\'','\'pintiliedenis02\'','\'dfa3fc67da63deeeecbc19b5aec613d9c9a1f599be0358ce08259ab5b4847aaa\'','\'RO67KALB3333777765654545\'','\'TZ-6721\'','\'denisflorin197@gmaill.com\'','\'Intrarea Doinei nr. 5\'','\'0732563214\'','\'06/06/1997\'','\'06/06/2029\'','\'Romania\'','\'roman\'','\'salariu\'','\'it-st\'');